
START TRANSACTION;

CREATE DATABASE pokedex;

CREATE TABLE pokemon (
  id int(11) NOT NULL,
  Nat int(11) ,
  Name text ,
  HP int(11) ,
  Attack int(11) ,
  Def int(11) ,
  Special_attack int(11) ,
  Special_def int(11) ,
  speed int(11) ,
  best int(11),
  PRIMARY KEY (id)
);


INSERT INTO pokemon (id, Nat, Name, HP, Attack, Def, Special_attack, Special_def, speed, best) VALUES
(1, 152, 'Chikorita', 45, 49, 65, 49, 65, 45, 318),
(2, 153, 'Bayleef', 60, 62, 80, 63, 80, 60, 405),
(3, 154, 'Meganium', 80, 82, 100, 83, 100, 80, 525),
(4, 155, 'Cyndaquil', 39, 52, 43, 60, 50, 65, 309),
(5, 156, 'Quilava', 58, 64, 58, 80, 65, 80, 405),
(6, 157, 'Typhlosion', 78, 84, 78, 109, 85, 100, 534),
(7, 158, 'Totodile', 50, 65, 64, 44, 48, 43, 314),
(8, 159, 'Croconaw', 65, 80, 80, 59, 63, 58, 405),
(9, 160, 'Feraligatr', 85, 105, 100, 79, 83, 78, 530),
(10, 161, 'Sentret', 35, 46, 34, 35, 45, 20, 215),
(11, 162, 'Furret', 85, 76, 64, 45, 55, 90, 415),
(12, 163, 'Hoothoot', 60, 30, 30, 36, 50, 56, 262),
(13, 164, 'Noctowl', 100, 50, 50, 76, 96, 70, 442),
(14, 165, 'Ledyba', 40, 20, 30, 40, 80, 55, 265),
(15, 166, 'Ledian', 55, 35, 50, 55, 110, 85, 390),
(16, 167, 'Spinirak', 40, 60, 40, 40, 40, 30, 250),
(17, 168, 'Ariados', 70, 90, 70, 60, 60, 40, 390),
(18, 169, 'Crobat', 85, 90, 80, 70, 80, 130, 535),
(19, 170, 'Chinchou', 75, 38, 38, 56, 56, 67, 330),
(20, 171, 'Lanturn', 125, 58, 58, 76, 76, 67, 460),
(21, 172, 'Pichu', 20, 40, 15, 35, 35, 60, 205),
(22, 173, 'Cleffa', 50, 25, 28, 45, 55, 15, 218),
(23, 174, 'Iggybuff', 90, 30, 15, 40, 20, 15, 210),
(24, 175, 'Togepi', 35, 20, 65, 40, 65, 20, 245),
(25, 176, 'Togetic', 55, 40, 85, 80, 105, 440, 405),
(26, 177, 'Natu', 40, 50, 45, 70, 45, 70, 320),
(27, 178, 'Xatu', 60, 75, 70, 95, 70, 95, 470),
(28, 179, 'Mareep', 55, 40, 40, 65, 45, 35, 280),
(29, 180, 'Flaaffy', 70, 55, 55, 80, 60, 45, 365),
(30, 181, 'Mega Ampharosu', 90, 95, 1055, 165, 110, 45, 610),
(31, 182, 'BEllossom', 75, 80, 95, 90, 100, 50, 490),
(32, 183, 'Marill', 70, 20, 50, 20, 50, 40, 250),
(33, 184, 'Azumarill', 100, 50, 80, 60, 80, 50, 420),
(34, 185, 'Sudowoodo', 70, 100, 115, 30, 65, 30, 410),
(35, 186, 'Politoed', 90, 75, 75, 90, 100, 70, 500),
(36, 187, 'Hoppip', 35, 35, 40, 35, 55, 50, 250),
(37, 188, 'Skiploom', 55, 45, 50, 45, 65, 80, 340),
(38, 189, 'Jumpluff', 75, 55, 70, 55, 95, 110, 460),
(39, 190, 'Aipom', 55, 70, 55, 40, 55, 85, 360),
(40, 191, 'Sunkern', 30, 30, 30, 30, 30, 30, 180),
(41, 192, 'Sunflora', 75, 75, 55, 105, 85, 30, 425),
(42, 193, 'Yanma', 65, 65, 45, 75, 45, 95, 390),
(43, 194, 'Wooper', 55, 45, 45, 25, 25, 15, 210),
(44, 195, 'Quagsire', 95, 85, 85, 65, 65, 35, 430),
(45, 196, 'Espeon', 65, 65, 60, 130, 95, 110, 525),
(46, 197, 'Umbreon', 95, 65, 110, 60, 130, 65, 525),
(47, 198, 'Murkrow', 60, 85, 42, 85, 42, 91, 405),
(48, 199, 'Slowking', 95, 75, 80, 100, 110, 30, 490),
(49, 200, 'Misdreavus', 60, 60, 60, 85, 85, 85, 435),
(50, 201, 'Unown', 48, 72, 48, 72, 48, 48, 336),
(51, 202, ' Wobbuffet', 190, 33, 58, 33, 58, 33, 405),
(52, 203, ' Girafarig', 70, 80, 65, 90, 65, 85, 455),
(53, 204, 'Pineco', 50, 65, 90, 35, 35, 15, 290),
(54, 205, 'Forretress', 75, 90, 140, 60, 60, 40, 465),
(55, 206, 'Dunsparce', 100, 70, 70, 65, 65, 45, 415),
(56, 207, 'Gligar', 65, 75, 105, 35, 65, 85, 430),
(57, 208, 'Steelix', 75, 85, 200, 55, 65, 30, 510),
(58, 208, 'Mega Steelix', 75, 125, 230, 55, 95, 30, 610),
(59, 209, 'Snubbull', 60, 80, 50, 40, 40, 30, 300),
(60, 210, 'Granbull', 90, 120, 75, 60, 60, 45, 450),
(61, 211, 'Qwilfish', 65, 95, 75, 55, 55, 85, 430),
(62, 212, 'Scizor', 70, 130, 100, 55, 80, 65, 500),
(63, 212, ' Mega Scizor', 70, 150, 140, 65, 100, 75, 600),
(64, 213, 'Shuckle', 20, 10, 230, 10, 230, 5, 505),
(65, 214, 'Heracross', 80, 125, 75, 40, 95, 85, 500),
(66, 214, 'Mega Heracross', 80, 185, 115, 40, 105, 75, 600),
(67, 215, 'Sneasel', 55, 95, 55, 35, 75, 115, 430),
(68, 216, 'Teddiursa', 60, 80, 50, 50, 50, 40, 330),
(69, 217, 'Ursaring', 90, 130, 75, 75, 75, 55, 500),
(70, 218, 'Slugma', 40, 40, 40, 70, 40, 20, 250),
(71, 218, 'Magcargo', 50, 50, 120, 80, 80, 30, 410);

 




CREATE TABLE type (
  id int(11) NOT NULL,
  type1 text NOT NULL,
  type2 text,
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES pokemon (id)
);




INSERT INTO type (id, type1, type2) VALUES
(1, 'Grass', NULL),
(2, 'Grass', NULL),
(3, 'Grass', NULL),
(4, 'Fire', NULL),
(5, 'Fire', NULL),
(6, 'Fire', NULL),
(7, 'Water', NULL),
(8, 'Water', NULL),
(9, 'Water', NULL),
(10, 'Normal', NULL),
(11, 'Normal', NULL),
(12, 'Normal', 'Flying'),
(13, 'Normal', 'Flying'),
(14, 'Bug', 'Flying'),
(15, 'Bug', 'Flying'),
(16, 'Bug', 'Poison'),
(17, 'Bug', 'Poison'),
(18, 'Poison', 'Flying'),
(19, 'Water', 'Electric'),
(20, 'Water', 'Electric'),
(21, 'Electric', NULL),
(22, 'Fairy', NULL),
(23, 'Normal', 'Fairy'),
(24, 'Fairy', NULL),
(25, 'Fairy', 'Flying'),
(26, 'Psychcic', 'Flying'),
(27, 'Psychcic', 'Flying'),
(28, 'Electric', NULL),
(29, 'Electric', NULL),
(30, 'Electric', 'Dragon'),
(31, 'Grass', NULL),
(32, 'Water', 'Fairy'),
(33, 'Water', 'Fairy'),
(34, 'Rock', NULL),
(35, 'Water', NULL),
(36, 'Grass', 'Flying'),
(37, 'Grass', 'Flying'),
(38, 'Grass', 'Flying'),
(39, 'Normal', NULL),
(40, 'Grass', NULL),
(41, 'Grass', NULL),
(42, 'Bug', 'Flying'),
(43, 'Water', 'Ground'),
(44, 'Water', 'Ground'),
(45, 'Pshychic', NULL),
(46, 'Dark', NULL),
(47, 'Dark', 'Flying'),
(48, 'Ghost', NULL),
(49, 'Ghost', NULL),
(50, 'Pshychic', NULL),
(51, 'Pshychic', NULL),
(52, 'Normal', 'Pshychic'),
(53, 'Bug', NULL),
(54, 'Bug', 'Steel'),
(55, 'Normal', NULL),
(56, 'Ground', 'flying'),
(57, 'Steel', 'Ground'),
(58, 'Steel', 'Ground'),
(59, 'Fariy', NULL),
(60, 'Fariy', NULL),
(61, 'Water', 'Poison'),
(62, 'Bug', 'Steel'),
(63, 'Bug', 'Steel'),
(64, 'Bug', 'Rock'),
(65, 'Bug', 'Fight'),
(66, 'Bug', 'Fight'),
(67, 'Dark', 'Ice'),
(68, 'Normal', NULL),
(69, 'Normal', NULL),
(70, 'Fire', NULL),
(71, 'Fire', 'Rock');
COMMIT;
